package com.zlz.common;

public class Result {
    public static final Result OK = Result.build((Object) null);

    public static final int COMMON_ERROR = 100;
    private String error;
    private boolean success=true;
    private Object data;
    private Integer errorCode;

    public static Result build(Object data) {
        Result result = new Result();
        result.data = data;
        return result;
    }

    public static Result build(int errorCode, String error) {
        Result result = new Result();
        result.errorCode = errorCode;
        result.error = error;
        result.success=false;
        return result;
    }

    public static Result build(Throwable e) {
        Result result = new Result();
        result.errorCode = COMMON_ERROR;
        result.success=false;
        if (e instanceof NullPointerException) {
            result.error = "空指针错误";
        } else {
            result.error = e.getMessage();
        }
        return result;
    }

    public Object getData() {
        return data;
    }

	public void setData(Object data) {
		this.data = data;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getError() {
		return error;
	}

	public Integer getErrorCode() {
		return errorCode;
	}
    
}
