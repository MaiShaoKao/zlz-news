package com.zlz.common;

/**
 * @author wangl
 * @desc
 * @date:2017/7/514:16
 * @Copyright (c) 2017, DaChen All Rights Reserved.
 */
public class OpenToken {

    private String userId;
    private String personId;
    private String openId;
    private String networkId;
    private String eid;
    private String oid;
    private String name;
    private String deviceId;
    private String deviceType;
    private String appClientId;
    private String uid;
    private String tid;
    private Long createTime;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getNetworkId() {
        return networkId;
    }

    public void setNetworkId(String networkId) {
        this.networkId = networkId;
    }

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getAppClientId() {
        return appClientId;
    }

    public void setAppClientId(String appClientId) {
        this.appClientId = appClientId;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "OpenToken{" +
            "userId='" + userId + '\'' +
            ", personId='" + personId + '\'' +
            ", openId='" + openId + '\'' +
            ", networkId='" + networkId + '\'' +
            ", eid='" + eid + '\'' +
            ", oid='" + oid + '\'' +
            ", name='" + name + '\'' +
            ", deviceId='" + deviceId + '\'' +
            ", deviceType='" + deviceType + '\'' +
            ", appClientId='" + appClientId + '\'' +
            ", uid='" + uid + '\'' +
            ", tid='" + tid + '\'' +
            ", createTime=" + createTime +
            '}';
    }
}
