package com.zlz.news.controller;

import com.zlz.common.Result;
import com.zlz.news.data.param.NewsParam;
import com.zlz.news.data.vo.NewsVO;
import com.zlz.news.data.vo.PageVO;
import com.zlz.news.service.INewsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 钟良
 * @desc
 * @date:2017/6/30 17:26 Copyright (c) 2017, DaChen All Rights Reserved.
 */
@Api(value = "新闻管理", description = "新闻管理", produces = MediaType.APPLICATION_JSON_VALUE, protocols = "http")
@RestController
@RequestMapping("zlz/news")
public class NewsController {
    private static final Logger logger = LoggerFactory.getLogger(NewsController.class);

    private static Map<String, NewsVO> newsMap = Collections.synchronizedMap(new HashMap<String, NewsVO>());

    @Autowired
    private INewsService newsService;

    @ApiOperation(value="新增新闻", notes="根据News对象新增新闻", response = Result.class)
    @ApiImplicitParam(name = "param", value = "新闻详细实体news", required = true, dataType = "NewsParam")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public Result postNews(@RequestBody @Valid NewsParam param) {
        logger.info("news newsParam：{}", param.toString());
        String newsId = newsService.save(param.toNews());
        NewsVO newsVO = newsService.getById(newsId);
        newsMap.put(newsId, newsVO);
        return Result.OK;
    }

    @ApiOperation(value="获取新闻列表", notes="获取新闻列表", response = NewsVO.class)
    @RequestMapping(value={""}, method = RequestMethod.GET)
    public Result getNewsList(
        @ApiParam(name = "pageIndex", value = "页码", required = true)
        @RequestParam(defaultValue = "0") Integer pageIndex,
        @ApiParam(name = "pageSize", value = "每页数据大小", required = true)
        @RequestParam(defaultValue = "15") Integer pageSize) {
        logger.debug("news pageIndex：{} pageSize：{}", pageIndex, pageSize);
        PageVO pageVO = new PageVO();
        pageVO.setPageIndex(pageIndex);
        pageVO.setPageSize(pageSize);
        newsMap.put("1001", new NewsVO("dev"));
        pageVO.setPageData(new ArrayList<>(newsMap.values()));
        pageVO.setPageCount(5);
        pageVO.setTotal(50L);
        return Result.build(pageVO);
    }

    @ApiOperation(value="获取新闻详细信息", notes="获取新闻详细信息", response = NewsVO.class)
    @RequestMapping(value={"/{id}"}, method = RequestMethod.GET)
    public Result getNews(@PathVariable String id) {
        logger.debug("news/top/{} ", id);
        return Result.build(newsMap.get(id));
    }

    @ApiOperation(value="按新闻标题搜索新闻", notes="获取新闻详细信息", response = NewsVO.class)
    @RequestMapping(value={"search/{title}"}, method = RequestMethod.GET)
    public Result searchNews(@PathVariable String title,
        @ApiParam(name = "pageIndex", value = "页码", required = true)
        @RequestParam(defaultValue = "0") Integer pageIndex,
        @ApiParam(name = "pageSize", value = "每页数据大小", required = true)
        @RequestParam(defaultValue = "15") Integer pageSize) {
        logger.debug("news/search/{} ", title);
        PageVO pageVO = new PageVO();
        pageVO.setPageIndex(pageIndex);
        pageVO.setPageSize(pageSize);
        newsMap.put("1001", new NewsVO("dev"));
        pageVO.setPageData(new ArrayList<>(newsMap.values()));
        pageVO.setPageCount(5);
        pageVO.setTotal(50L);
        return Result.build(pageVO);
    }

    @ApiOperation(value="编辑新闻", notes="编辑新闻", response = Result.class)
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Result putNews(@PathVariable String id) {
        logger.debug("news/{} ", id);
        return Result.OK;
    }

    @ApiOperation(value="置顶新闻", notes="置顶新闻", response = Result.class)
    @RequestMapping(value = "top/{id}", method = RequestMethod.PUT)
    public Result topNews(@ApiParam("新闻Id") @PathVariable String id) {
        logger.debug("news/top/{} ", id);
        return Result.OK;
    }

    @ApiOperation(value="发布新闻", notes="发布新闻", response = Result.class)
    @RequestMapping(value = "publish/{id}", method = RequestMethod.PUT)
    public Result publishNews(@PathVariable String id) {
        logger.debug("news/publish/{} ", id);
        return Result.OK;
    }

    @ApiOperation(value="删除新闻", notes="删除新闻", response = Result.class)
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Result deleteNews(@PathVariable String id) {
        logger.debug("DELETE news/{} ", id);
        return Result.OK;
    }
}
