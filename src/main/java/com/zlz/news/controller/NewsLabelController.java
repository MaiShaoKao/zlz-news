package com.zlz.news.controller;

import com.zlz.news.constant.MediaType;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 钟良
 * @desc
 * @date:2017/7/11 10:04 Copyright (c) 2017, DaChen All Rights Reserved.
 */
@Api(value = "新闻标签库", description = "新闻标签库", produces = MediaType.APPLICATION_JSON_VALUE, protocols = "http")
@RestController
@RequestMapping("news/label")
public class NewsLabelController {
    private static final Logger logger = LoggerFactory.getLogger(NewsLabelController.class);

}
