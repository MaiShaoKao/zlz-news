package com.zlz.news.component;


import com.zlz.news.constant.Constants;
import com.zlz.news.enums.ExceptionEunm;
import com.zlz.news.exception.ServiceException;
import com.zlz.news.util.CacheUtil;
import com.zlz.news.util.SpringBeansUtils;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author 钟良
 * @desc
 * @date:2017/7/7 09:36 Copyright (c) 2017, DaChen All Rights Reserved.
 */
public class TokenInteceptor implements HandlerInterceptor {
    private static final Logger logger = LoggerFactory.getLogger(TokenInteceptor.class);
    private JedisWrap jedisWrap;

    public TokenInteceptor(){
        if (jedisWrap == null){
            jedisWrap = SpringBeansUtils.getBean(JedisWrap.class);
        }
    }

    @Override
    public boolean preHandle(HttpServletRequest req, HttpServletResponse resp, Object handler) throws Exception {
        printRequestParams(req);
        String access_token = req.getHeader("openToken");
        if(StringUtils.isBlank(access_token))
            throw new ServiceException(ExceptionEunm.TOKEN_INVALID.getCode(), ExceptionEunm.TOKEN_INVALID.getMessage());
        String key= Constants.REDIS_TOKEN_KEY+access_token;
        String openTokenJson = jedisWrap.get(key);
        if(StringUtils.isBlank(openTokenJson))
            throw new ServiceException(ExceptionEunm.TOKEN_ERROR.getCode(), ExceptionEunm.TOKEN_ERROR.getMessage());
        CacheUtil.put(access_token, openTokenJson);
        return true;
    }

    private void printRequestParams(HttpServletRequest request) {
        StringBuilder sb = new StringBuilder();
        sb.append("请求：" + request.getRequestURI());
        sb.append("\n");
        Map<String, String[]> paramMap = request.getParameterMap();
        if (!paramMap.isEmpty()) {
            sb.append("?");
            for (String key : paramMap.keySet()) {
                sb.append(key).append("=").append(paramMap.get(key)[0]).append("&");
            }
            sb.setLength(sb.length() - 1);
            sb.append("\n");
        }
        sb.append("Method：" + request.getMethod());
        sb.append("\n");
        sb.append("Content-Type：" + request.getContentType());
        sb.append("\n");
        sb.append("User-Agent：" + request.getHeader("User-Agent"));
        sb.append("\n");
        sb.append("**************************************************");
        sb.append("\n");
        logger.info(sb.toString());
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest,
        HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView)
        throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest,
        HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
