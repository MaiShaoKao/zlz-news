package com.zlz.news.dao.impl;

import com.zlz.news.annotation.Model;
import com.zlz.news.dao.INewsDao;
import com.zlz.news.data.entity.News;
import org.springframework.stereotype.Repository;

/**
 * @author 钟良
 * @desc
 * @date:2017/6/30 10:55 Copyright (c) 2017, DaChen All Rights Reserved.
 */
@Model(News.class)
@Repository
public class NewsDaoImpl extends BaseDaoImpl implements INewsDao {

}
