package com.zlz.news.dao.impl;

import com.mongodb.WriteResult;
import com.zlz.news.annotation.Model;
import com.zlz.news.dao.IBaseDao;
import com.zlz.news.exception.ServiceException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.mongodb.morphia.AdvancedDatastore;
import org.mongodb.morphia.Key;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.mapping.Mapper;
import org.mongodb.morphia.query.Criteria;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.mongodb.morphia.query.UpdateResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.CollectionUtils;

/**
 * @author 钟良
 * @desc
 * @date:2017/6/30 10:20 Copyright (c) 2017, DaChen All Rights Reserved.
 */
public abstract class BaseDaoImpl implements InitializingBean, IBaseDao {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource(name = "dsForRW")
    protected AdvancedDatastore dsForRW;

    protected Class<?> entityClass;

    public BaseDaoImpl() {
        Model annotation = this.getClass().getAnnotation(Model.class);

        this.entityClass = annotation.value();
    }

    @Override
    public void afterPropertiesSet() throws Exception {

    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T findById(String id) {
        Query<T> query = this.createQueryByPK(id);
        return query.get();
    }

    @Override
    public <T> List<T> findByIds(List<String> ids) {
        Query<T> query = this.createQueryByPKs(ids);
        return query.asList();
    }

    @Override
    public <T> Query<T> createQueryByPKs(List<String> idList) {
        if (CollectionUtils.isEmpty(idList)) {
            throw new ServiceException("idList is empty!!!");
        }
        Query<T> query = this.createQuery();
        Criteria[] idParams = new Criteria[idList.size()];
        for (int i = 0; i < idList.size(); i++) {
            idParams[i] = query.criteria(Mapper.ID_KEY).equal(new ObjectId(idList.get(i)));
        }
        query.or(idParams);
//        query.field(Mapper.ID_KEY).in(idList);    // 不能用in，ObjectId.in strings？
        return query;
    }

    @Override
    public <T> Query<T> createQueryByPK(String id) {
        if (StringUtils.isBlank(id)) {
            throw new ServiceException("id is blank!!!");
        }
        Query<T> query = this.createQuery();
        query.field(Mapper.ID_KEY).equal(new ObjectId(id));
        return query;
    }


    @Override
    @SuppressWarnings("unchecked")
    public <T> Query<T> createQuery() {
        return (Query<T>) this.dsForRW.createQuery(this.entityClass);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> UpdateOperations<T> createUpdateOperations() {
        return (UpdateOperations<T>) this.dsForRW.createUpdateOperations(this.entityClass);
    }

    /**
     * 此方法不推荐使用！有可能存在的脏数据覆盖新数据的问题。
     * 如果要更新某些字段，请使用update(String id, Map<String, Object> map)方法
     * 如果确实要调用此方法，请先调用findById获取最新的entity再调用saveEntity
     *
     * @param entity
     * @param <T>
     * @return
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T> String saveEntity(T entity) {
        if (null == entity) {
            throw new ServiceException("entity is null!!!");
        }
        return dsForRW.save(entity).getId().toString();
    }

    /**
     * 此方法不推荐使用！有可能存在的脏数据覆盖新数据的问题。
     * 如果要更新某些字段，请使用update(String id, Map<String, Object> map)方法
     * 如果确实要调用此方法，请先调用findById获取最新的entity再调用saveEntity
     *
     * @param entity
     * @param <T>
     * @return
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T> T saveEntityAndFind(T entity) {
        String id = dsForRW.save(entity).getId().toString();
        return (T) findById(id);
    }

    @Override
    public <T> List<String> saveEntityBatch(List<T> entityList) {
        if (CollectionUtils.isEmpty(entityList)) {
            return null;
        }
        List<String> idList = new ArrayList<String>(entityList.size());
        Iterator<Key<T>> iterator = this.dsForRW.save(entityList).iterator();
        while (iterator.hasNext()) {
            idList.add(iterator.next().getId().toString());
        }
        return idList;
    }

    @Override
    public <T> List<T> saveEntityBatchAndFind(List<T> entityList) {
        if (CollectionUtils.isEmpty(entityList)) {
            return null;
        }
        Iterable<Key<T>> ret = this.dsForRW.save(entityList);
        Iterator<Key<T>> iter = ret.iterator();
        List<String> idList = new ArrayList<String>(entityList.size());
        while (iter.hasNext()) {
            String id = iter.next().getId().toString();
            idList.add(id);
        }
        return findByIds(idList);
    }


    @Override
    public <T> boolean update(String id, Map<String, Object> map) {
        if (CollectionUtils.isEmpty(map)) {
            return false;
        }

        Query<T> query = this.createQueryByPK(id);

        boolean needUpdate = false;

        UpdateOperations<T> ops = this.createUpdateOperations();
        // use entrySet rather than keySet to iterate
        Iterator<Map.Entry<String, Object>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Object> entry = iterator.next();
            String key = entry.getKey();
            Object val = entry.getValue();
            if (null != val) {
                ops.set(key, val);
                needUpdate = true;
            }
//            if (StringUtils.isNotBlank(key)) {
//                ops.set(key, Objects.toString(val, ""));    // 支持值置空（此语句有错，by xiaowei, 170406）
//                needUpdate = true;
//            }
        }

        if (!needUpdate) {
            return false;
        }

        UpdateResults ur = dsForRW.update(query, ops);
        return ur.getUpdatedCount()>0;
    }

    @Override
    public <T> int update(final Query<T> q, final UpdateOperations<T> ops) {
        // TODO: 判断T是否存在updateCreate字段，如有，更新之
        return this.dsForRW.update(q, ops).getUpdatedCount();
    }

    @Override
    public <T> int update(String id, String fieldName, Object fieldValue) {
        Query<T> query = this.createQueryByPK(id);

        UpdateOperations<T> ops = this.createUpdateOperations();
//        ops.set(fieldName, Objects.toString(fieldValue, "")); // 不能toString, fieldValue类型不一定是String，也可能是Integer
        if (null == fieldValue) {
            ops.set(fieldName, "");
        } else {
            ops.set(fieldName, fieldValue);
        }

        return this.update(query, ops);
    }

    @Override
    public <T> int update(List<String> idList, String fieldName, Object fieldValue) {
        Query<T> query = this.createQueryByPKs(idList);

        UpdateOperations<T> ops = this.createUpdateOperations();
//        ops.set(fieldName, Objects.toString(fieldValue, ""));
        if (null == fieldValue) {
            ops.set(fieldName, "");
        } else {
            ops.set(fieldName, fieldValue);
        }

        return this.update(query, ops);
    }

    @Override
    public <T> int deleteByQuery(Query<T> query) {
        return dsForRW.delete(query).getN();
    }

    @Override
    public <T> int deleteByIds(List<String> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return 0;
        }
        Query<T> query = this.createQueryByPKs(ids);
        return this.deleteByQuery(query);
    }

    @Override
    public <T> boolean deleteById(String id) {
        Query<T> query = this.createQueryByPK(id);
        WriteResult wr = dsForRW.delete(query);
        return wr.getN()>0;
    }
}
