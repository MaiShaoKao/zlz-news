package com.zlz.news.dao;

import java.util.List;
import java.util.Map;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;

/**
 * @author 钟良
 * @desc
 * @date:2017/6/30 10:21 Copyright (c) 2017, DaChen All Rights Reserved.
 */
public interface IBaseDao {
    <T> T findById(String id);
    <T> List<T> findByIds(List<String> ids);

    <T> String saveEntity(T entity);

    <T> T saveEntityAndFind(T entity);
    <T> List<String> saveEntityBatch(List<T> entityList);

    <T> List<T> saveEntityBatchAndFind(List<T> entityList);

    <T> Query<T> createQuery();

    <T> Query<T> createQueryByPKs(List<String> idList);
    <T> Query<T> createQueryByPK(String id);
    <T> UpdateOperations<T> createUpdateOperations();

    <T> int update(Query<T> q, UpdateOperations<T> ops);
    <T> boolean update(String id, Map<String, Object> map);
    <T> int update(String id, String fieldName, Object fieldValue);
    <T> int update(List<String> idList, String fieldName, Object fieldValue);


    <T> int deleteByQuery(Query<T> q);
    <T> int deleteByIds(List<String> ids);
    <T> boolean deleteById(String id);
}
