package com.zlz.news.data.vo;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author 钟良
 * @desc
 * @date:2017/7/7 17:27 Copyright (c) 2017, DaChen All Rights Reserved.
 */
public class NewsVO {
    @ApiModelProperty(value = "新闻Id")
    private String id;
    @ApiModelProperty(value = "标题")
    private String title;
    @ApiModelProperty(value = "浏览量")
    private Long pageview;
    @ApiModelProperty(value = "发布人")
    private String publisher;
    @ApiModelProperty(value = "发布时间")
    private Long publishTime;
    @ApiModelProperty(value = "是否发布", notes = "是否发布：1已发布 0未发布")
    private Boolean released;

    public NewsVO() {

    }

    public NewsVO(String dev) {
        this.id = "新闻Id";
        this.title = "标题";
        this.pageview = 20L;
        this.publisher = "发布人";
        this.publishTime = 1499407607492L;
        this.released = false;
    }

    public NewsVO(String id, String title, Long pageview, String publisher, Long publishTime,
        Boolean released) {
        this.id = id;
        this.title = title;
        this.pageview = pageview;
        this.publisher = publisher;
        this.publishTime = publishTime;
        this.released = released;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getPageview() {
        return pageview;
    }

    public void setPageview(Long pageview) {
        this.pageview = pageview;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Long getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Long publishTime) {
        this.publishTime = publishTime;
    }

    public Boolean getReleased() {
        return released;
    }

    public void setReleased(Boolean released) {
        this.released = released;
    }
}
