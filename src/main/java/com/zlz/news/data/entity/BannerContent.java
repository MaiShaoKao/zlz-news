package com.zlz.news.data.entity;

import org.mongodb.morphia.annotations.Embedded;

/**
 * @author 钟良
 * @desc
 * @date:2017/7/5 11:44 Copyright (c) 2017, DaChen All Rights Reserved.
 */
@Embedded
public class BannerContent {
    /**
     * 名称
     */
    private String name;
    /**
     * 图像地址
     */
    private String imgurl;
    /**
     * 链接地址
     */
    private String linkurl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public String getLinkurl() {
        return linkurl;
    }

    public void setLinkurl(String linkurl) {
        this.linkurl = linkurl;
    }
}
