package com.zlz.news.data.entity;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author 钟良
 * @desc
 * @date:2017/7/5 11:30 Copyright (c) 2017, DaChen All Rights Reserved.
 */
@Entity(value = "t_zlz_news_quantity", noClassnameStored = true)
public class NewsQuantity {
    @Id
    private String id;
    /**
     * 新闻Id
     */
    private String newsId;
    /**
     * 浏览量
     */
    private Long pageview;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNewsId() {
        return newsId;
    }

    public void setNewsId(String newsId) {
        this.newsId = newsId;
    }

    public Long getPageview() {
        return pageview;
    }

    public void setPageview(Long pageview) {
        this.pageview = pageview;
    }
}
