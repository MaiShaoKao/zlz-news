package com.zlz.news.data.entity;

import com.zlz.news.data.param.NewsParam;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author 钟良
 * @desc
 * @date:2017/6/29 10:05 Copyright (c) 2017, DaChen All Rights Reserved.
 */
@Entity(value = "t_news", noClassnameStored = true)
public class News {
    @Id
    private String id;
    /**
     * 新闻内容相关信息
     */
    @Embedded
    private NewsContent newsContent;
    /**
     * 创建信息
     */
    @Embedded
    private Creation creation;
    /**
     * 发布人Id
     */
    private String publisherId;
    /**
     * 发布时间
     */
    private Long publishTime;
    /**
     * 置顶标识
     */
    private Long top;
    /**
     * 是否发布
     */
    private Boolean released;
    /**
     * 是否删除
     */
    private Boolean deleted;

    public News(){}

    public News(NewsParam param){
        NewsContent content = new NewsContent();
        setNewsContent(content);
        content.setTitle(param.getTitle());
        content.setAuthor(param.getAuthor());
        content.setImgurl(param.getImgurl());
        content.setContent(param.getContent());
        content.setLabels(param.getLabels());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public NewsContent getNewsContent() {
        return newsContent;
    }

    public void setNewsContent(NewsContent newsContent) {
        this.newsContent = newsContent;
    }

    public Creation getCreation() {
        return creation;
    }

    public void setCreation(Creation creation) {
        this.creation = creation;
    }

    public String getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(String publisherId) {
        this.publisherId = publisherId;
    }

    public Long getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Long publishTime) {
        this.publishTime = publishTime;
    }

    public Long getTop() {
        return top;
    }

    public void setTop(Long top) {
        this.top = top;
    }

    public Boolean getReleased() {
        return released;
    }

    public void setReleased(Boolean released) {
        this.released = released;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
