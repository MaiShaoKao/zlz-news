package com.zlz.news.data.entity;

import org.mongodb.morphia.annotations.Embedded;

/**
 * @author 钟良
 * @desc
 * @date:2017/7/5 11:59 Copyright (c) 2017, DaChen All Rights Reserved.
 */
@Embedded
public class Creation {
    /**
     * 创建人Id
     */
    private String creatorId;
    /**
     * 创建时间
     */
    private Long createTime;
    /**
     * 最后更新人Id
     */
    private String updatorId;
    /**
     * 最后更新时间
     */
    private Long updateTime;

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getUpdatorId() {
        return updatorId;
    }

    public void setUpdatorId(String updatorId) {
        this.updatorId = updatorId;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }
}
