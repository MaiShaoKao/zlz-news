package com.zlz.news.data.entity;

import java.util.List;
import org.mongodb.morphia.annotations.Embedded;

/**
 * @author 钟良
 * @desc
 * @date:2017/7/5 11:08 Copyright (c) 2017, DaChen All Rights Reserved.
 */
@Embedded
public class NewsContent {
    /**
     * 标题
     */
    private String title;
    /**
     * 作者
     */
    private String author;
    /**
     * 缩略图
     */
    private String imgurl;
    /**
     * 正文
     */
    private String content;
    /**
     * H5地址
     */
    private String h5Url;
    /**
     * 标签
     */
    private List<String> labels;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getH5Url() {
        return h5Url;
    }

    public void setH5Url(String h5Url) {
        this.h5Url = h5Url;
    }

    public List<String> getLabels() {
        return labels;
    }

    public void setLabels(List<String> labels) {
        this.labels = labels;
    }
}
