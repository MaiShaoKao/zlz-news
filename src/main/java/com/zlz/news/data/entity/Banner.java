package com.zlz.news.data.entity;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author 钟良
 * @desc
 * @date:2017/7/5 11:32 Copyright (c) 2017, DaChen All Rights Reserved.
 */
@Entity(value = "t_zlz_news_banner")
public class Banner {
    @Id
    private String id;
    /**
     * 内容，内嵌文档
     */
    @Embedded
    private BannerContent bannerContent;
    /**
     * 排序
     */
    private Long sort;
    /**
     * 是否删除
     */
    private Boolean deleted;
    /**
     * 创建信息
     */
    @Embedded
    private Creation creation;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BannerContent getBannerContent() {
        return bannerContent;
    }

    public void setBannerContent(BannerContent bannerContent) {
        this.bannerContent = bannerContent;
    }

    public Long getSort() {
        return sort;
    }

    public void setSort(Long sort) {
        this.sort = sort;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Creation getCreation() {
        return creation;
    }

    public void setCreation(Creation creation) {
        this.creation = creation;
    }
}
