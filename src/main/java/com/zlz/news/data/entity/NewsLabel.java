package com.zlz.news.data.entity;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author 钟良
 * @desc
 * @date:2017/7/5 11:26 Copyright (c) 2017, DaChen All Rights Reserved.
 */
@Entity(value = "t_zlz_news_label", noClassnameStored = true)
public class NewsLabel {
    @Id
    private String id;
    /**
     * 标签名称
     */
    private String name;
    /**
     * 创建信息
     */
    @Embedded
    private Creation creation;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Creation getCreation() {
        return creation;
    }

    public void setCreation(Creation creation) {
        this.creation = creation;
    }
}
