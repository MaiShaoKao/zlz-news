package com.zlz.news.data.param;

import com.zlz.news.data.entity.News;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author 钟良
 * @desc
 * @date:2017/7/5 15:08 Copyright (c) 2017, DaChen All Rights Reserved.
 */
public class NewsParam {
    @NotBlank(message = "标题不能为空")
    @ApiModelProperty(value = "标题")
    private String title;
    @NotBlank(message = "作者不能为空")
    @ApiModelProperty(value = "作者")
    private String author;
    @NotBlank(message = "缩略图不能为空")
    @ApiModelProperty(value = "缩略图")
    private String imgurl;
    @NotBlank(message = "正文不能为空")
    @ApiModelProperty(value = "正文")
    private String content;
    @NotEmpty(message = "标签不能为空")
    @ApiModelProperty(value = "标签")
    private List<String> labels;

    public News toNews(){
        return new News(this);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<String> getLabels() {
        return labels;
    }

    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    @Override
    public String toString() {
        return "NewsParam{" +
            "title='" + title + '\'' +
            ", author='" + author + '\'' +
            ", imgurl='" + imgurl + '\'' +
            ", content='" + content + '\'' +
            ", labels=" + labels +
            '}';
    }
}
