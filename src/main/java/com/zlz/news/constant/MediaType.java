package com.zlz.news.constant;

/**
 * @author 钟良
 * @desc
 * @date:2017/7/7 17:09 Copyright (c) 2017, DaChen All Rights Reserved.
 */
public class MediaType {
    public static final String APPLICATION_JSON_VALUE = "application/json";
}
