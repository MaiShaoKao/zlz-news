package com.zlz.news.constant;

/**
 * @author wangl
 * @desc
 * @date:2017/7/419:52
 * @Copyright (c) 2017, DaChen All Rights Reserved.
 */
public class Constants {

    public static final String REDIS_TOKEN_KEY = "zlz:open_token:";
    public static final String HEADER_TOKEN_KEY = "openToken";
    public static final String ERROR_CODE = "errorCode";
    public static final String ERROR = "error";
    public static final String SUCCESS = "success";
    public static final String DATA = "data";
}
