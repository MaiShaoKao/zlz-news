package com.zlz.news.service.impl;

import com.zlz.news.dao.INewsDao;
import com.zlz.news.data.entity.Creation;
import com.zlz.news.data.entity.News;
import com.zlz.news.data.vo.NewsVO;
import com.zlz.news.service.INewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 钟良
 * @desc
 * @date:2017/6/30 17:25 Copyright (c) 2017, DaChen All Rights Reserved.
 */
@Service
public class NewsServiceImpl implements INewsService {
    @Autowired
    private INewsDao newsDao;

    @Override
    public String save(News news) {
        setCreation(news);
        setStatus(news);
        return newsDao.saveEntity(news);
    }

    private void setStatus(News news) {
        news.setDeleted(Boolean.FALSE);
        news.setTop(0L);
        news.setReleased(Boolean.FALSE);
    }

    private void setCreation(News news) {
        Creation creation = new Creation();
        creation.setCreatorId("zl001");
        long curTime = System.currentTimeMillis();
        creation.setCreateTime(curTime);
        creation.setUpdatorId("zl001");
        creation.setUpdateTime(curTime);
        news.setCreation(creation);
    }

    @Override
    public NewsVO getById(String newsId) {
        return newsDao.findById(newsId);
    }
}
