package com.zlz.news.service;

import com.zlz.news.data.entity.News;
import com.zlz.news.data.vo.NewsVO;

/**
 * @author 钟良
 * @desc
 * @date:2017/6/30 17:25 Copyright (c) 2017, DaChen All Rights Reserved.
 */
public interface INewsService {
    String save(News news);
    NewsVO getById(String newsId);
}
