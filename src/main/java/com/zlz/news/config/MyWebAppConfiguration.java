package com.zlz.news.config;

import com.zlz.news.component.TokenInteceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author 钟良
 * @desc
 * @date:2017/7/6 13:51 Copyright (c) 2017, DaChen All Rights Reserved.
 */
public class MyWebAppConfiguration extends WebMvcConfigurerAdapter {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 多个拦截器组成一个拦截器链
        // addPathPatterns 用于添加拦截规则
        // excludePathPatterns 用户排除拦截
        registry.addInterceptor(new TokenInteceptor()).addPathPatterns("/**");
        super.addInterceptors(registry);
    }
}
