package com.zlz.news.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by Administrator on 2017/3/6.
 */
@Component
@ConfigurationProperties(prefix = "redis")
public class RedisConfig {

    private String sentinels;
    private int timeout = 2000;
    private String master;
    private String password;
    private int database = 2;

    public String getSentinels() {
        return sentinels == null ? sentinels : sentinels.trim();
    }

    public void setSentinels(String sentinels) {
        this.sentinels = sentinels;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public String getMaster() {
        return master;
    }

    public void setMaster(String master) {
        this.master = master;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getDatabase() {
        return database;
    }

    public void setDatabase(int database) {
        this.database = database;
    }

}
