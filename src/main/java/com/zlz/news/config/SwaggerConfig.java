package com.zlz.news.config;

import com.google.common.base.Predicate;
import com.zlz.news.constant.MediaType;
import io.swagger.annotations.ApiOperation;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.boot.autoconfigure.web.BasicErrorController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 
 * 	@Api：修饰整个类，描述Controller的作用
	@ApiOperation：描述一个类的一个方法，或者说一个接口
	@ApiParam：单个参数描述
	@ApiModel：用对象来接收参数
	@ApiProperty：用对象接收参数时，描述对象的一个字段
	
	@ApiResponse：HTTP响应其中1个描述
	@ApiResponses：HTTP响应整体描述
	@ApiIgnore：使用该注解忽略这个API 
 *
 */
@Configuration
@EnableSwagger2
@Profile({"dev","test"})
public class SwaggerConfig {

	@Bean
    public Docket createRestApi() {
        Predicate<RequestHandler> predicate = new Predicate<RequestHandler>() {
            @Override
            public boolean apply(RequestHandler input) {
                Class<?> declaringClass = input.declaringClass();
                if (declaringClass == BasicErrorController.class)// 排除
                    return false;
                if(declaringClass.isAnnotationPresent(RestController.class)) // 被注解的类
                    return true;
                if(input.isAnnotatedWith(ResponseBody.class)) // 被注解的方法
                    return true;
                return false;
            }
        };
        
        Predicate<RequestHandler> predicate1 = RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class);
        
        ParameterBuilder tokenPar = new ParameterBuilder();  
        List<Parameter> pars = new ArrayList<Parameter>();  
        tokenPar.name("openToken").description("令牌").modelRef(new ModelRef("string")).parameterType("header").required(true).build();  
        pars.add(tokenPar.build());  
        
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName("用户管理")
//                .consumes(consumes())
                .produces(produces())//请求响应格式
                .useDefaultResponseMessages(false)
                .globalOperationParameters(pars)
                .select()
                .apis(predicate1)
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("致良知-新闻管理API接口")
            .description(desc())
            .contact(new Contact("钟良", "", ""))
            .version("1.0")//版本
            .licenseUrl("haha")
            .build();
    }
    
    private String desc(){
    	StringBuffer sb = new StringBuffer();
    	sb.append("所有接口均返回以下格式的json数据：\n")
    	  .append("{\n")
    	  .append(" \"errorCode\":\"错误码,整型\",\n")
    	  .append(" \"error\":\"错误信息\",\n")
    	  .append(" \"success\":\"true or false,布尔。表示请求是否成功\",\n")
    	  .append(" \"data\": {\n")
    	  .append("  \"成功时返回的信息[json对象]，如无特殊说明，所有接口的响应信息 (状态码 200)只提供此部分内容；\"\n")
    	  .append("}");
    	return sb.toString();
    }
    
    private Set<String>produces() {
    	Set<String>set = new HashSet<>();
    	set.add(MediaType.APPLICATION_JSON_VALUE);
    	return set;
    }
}
