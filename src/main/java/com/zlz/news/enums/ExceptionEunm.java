package com.zlz.news.enums;

/**
 * @author wangl
 * @desc
 * @date:2017/7/510:31
 * @Copyright (c) 2017, DaChen All Rights Reserved.
 */
public enum ExceptionEunm {

    COMMON_ERROR(100,"服务器异常"),
    TOKEN_INVALID(1030102,"访问令牌不存在"),
    TOKEN_ERROR(100001, "访问令牌错误"),
    PARAM_ERROR(100200, "请求参数错误");


    private int code;
    private String message;

    ExceptionEunm(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
