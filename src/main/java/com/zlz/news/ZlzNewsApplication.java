package com.zlz.news;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@ComponentScan("com.zlz.news")
@EnableAsync
public class ZlzNewsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZlzNewsApplication.class, args);
	}
}
